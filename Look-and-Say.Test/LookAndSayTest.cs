﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LookAndSay.Test
{
	[TestClass]
	public class LookAndSayTest
	{
		[TestMethod]
		[TestCategory("Nível 1")]
		public void Dado_1211_deve_retornoar_111221()
		{
			// Arrange

			// Act
			var act = LookAndSay.Say("1211");

			// Asserts
			Assert.AreEqual("111221", act);
		}

		[TestMethod]
		[TestCategory("Nível 1")]
		public void Dado_111221_deve_retornoar_312211()
		{
			// Arrange

			// Act
			var act = LookAndSay.Say("111221");

			// Asserts
			Assert.AreEqual("312211", act);
		}

		[TestMethod]
		[TestCategory("Nível 1")]
		public void Dado_13211d_deve_retornoar_111312211d()
		{
			// Arrange

			// Act
			var act = LookAndSay.Say("13211d");

			// Asserts
			Assert.AreEqual("111312211d", act);
		}

		[TestMethod]
		[TestCategory("Nível 2")]
		public void Iniciando_com_1_elemento_posicao_5_retornar_312211()
		{
			// Arrange
			var say = new LookAndSay("1");

			// Act
			var act = say.Elemento(5);

			// Asserts
			Assert.AreEqual("312211", act);
		}

		[TestMethod]
		[TestCategory("Nível 2")]
		public void Iniciando_com_a_elemento_posicao_5_retornar_312211()
		{
			// Arrange
			var say = new LookAndSay("a");

			// Act
			var act = say.Elemento(5);

			// Asserts
			Assert.AreEqual("111312211a", act);
		}

		[TestMethod]
		[TestCategory("Nível 3")]
		public void Dado_o_digito_x_soma_dos_digitos_no_elemento_5_e_igual_13()
		{
			// Arrange
			var say = new LookAndSay("x");

			// Act
			var ele = say.Elemento(5);
			var soma = ele.SomarDigitos();

			// Asserts
			Assert.AreEqual(13, soma);
		}
	}
}
