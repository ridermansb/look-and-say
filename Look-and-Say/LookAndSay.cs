﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LookAndSay
{
	public class LookAndSay
	{
		public List<string> Elementos { get; private set; }

		public LookAndSay(string start)
		{
			Elementos = new List<string>();
			Elementos.Add(start);
		}

		public string ProximoElemento()
		{
			var say = LookAndSay.Say(Elementos[Elementos.Count - 1]);
			Elementos.Add(say);
			return say;
		}

		public string Elemento(int posicao)
		{
			if (Elementos.Count - 1 < posicao)
			{
				var elementAnt = Elemento(posicao - 1);
				return ProximoElemento();
			}

			return Elementos[posicao];
		}

		public static string Say(string act)
		{
			var strLock = new StringBuilder();
			var arrDigitos = new ArrayList(act.ToList());
			var i = 0;
			while (arrDigitos.Count > 0)
			{
				// Caso exista um próximo item e este seja igual ao item atual, avança para o próximo item.
				if (arrDigitos.Count != i + 1 && arrDigitos[i].ToString() == arrDigitos[i + 1].ToString())
				{
					i++;
					continue;
				}

				if (strLock.Length != 0)
					strLock.Append(",");

				// Adiciona a sequencia na strLock e após uma virgula para separar dos demais itens.
				strLock.Append(string.Join("", arrDigitos.GetRange(0, i + 1).ToArray()));
				arrDigitos.RemoveRange(0, i + 1);
				i = 0;
			}

			// Formata o resultado (count + digito)
			var strSay = new StringBuilder();
			foreach (var item in strLock.ToString().Split(','))
				strSay.Append(item.Count().ToString() + item[0].ToString());

			return strSay.ToString();
		}
	}
}
