﻿using System.Linq;
using System.Text.RegularExpressions;

namespace LookAndSay
{
	public static class Extensions
	{
		public static int SomarDigitos(this string src)
		{
			var match = Regex.Match(src, @"\d+");
			var res = 0;
			if (match.Success)
			{
				int val;
				if (int.TryParse(match.Value, out val))
				{
					foreach (var item in val.ToString().ToArray())
						res += int.Parse(item.ToString());
				}
			}

			return res;
		}
	}
}
